const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');

const EESeedToken = artifacts.require('EESeedToken');

function ether(value) {
    return web3.utils.toWei(value, 'ether');
}

// State persists sequencially for each test
contract('EESeedToken', accounts => {
    const ADMIN = accounts[0]
    const BENEFICIARY = accounts[5]
    const CLIENT = accounts[1]
    const RANDOM = accounts[2]

    it('should allow management of recipients by admin', async() => {
        const seedToken = await EESeedToken.deployed()
        assert.equal(await seedToken.whitelistedRecipients.call(BENEFICIARY, { from: ADMIN }), false, "already allowed")
        await seedToken.recipientPermissionManager.sendTransaction(BENEFICIARY, true, { from: ADMIN })
        assert.equal(await seedToken.whitelistedRecipients.call(BENEFICIARY, { from: ADMIN }), true, "not allowed")
        await seedToken.recipientPermissionManager.sendTransaction(BENEFICIARY, false, { from: ADMIN })
        assert.equal(await seedToken.whitelistedRecipients.call(BENEFICIARY, { from: ADMIN }), false, "allowance not revoked")
    })

    it('should not allow management of recipients by random user', async() => {
        const seedToken = await EESeedToken.deployed()
        await truffleAssert.fails(
            seedToken.recipientPermissionManager.sendTransaction(BENEFICIARY, true, { from: RANDOM }),
            null,
            "must have admin role to whitelist"
        );
    })

    it('should be pausible', async() => {
        const seedToken = await EESeedToken.deployed()
        assert.equal(await seedToken.paused.call({ from: CLIENT }), false, "already paused")
        await seedToken.pause.sendTransaction({ from: ADMIN })
        assert.equal(await seedToken.paused.call({ from: CLIENT }), true, "not paused")
        await seedToken.recipientPermissionManager.sendTransaction(BENEFICIARY, true, { from: ADMIN })

        await truffleAssert.fails(
            seedToken.transfer.sendTransaction(BENEFICIARY, ether("50000"), { from: CLIENT }),
            null,
            "ERC20Pausable: token transfer while paused"
        );

        await truffleAssert.fails(
            seedToken.transferFrom.sendTransaction(CLIENT, BENEFICIARY, ether("50000"), { from: CLIENT }),
            null,
            "ERC20Pausable: token transfer while paused"
        );

        await seedToken.unpause.sendTransaction({ from: ADMIN })
        assert.equal(await seedToken.paused.call({ from: CLIENT }), false, "still paused")
    })

    it('should not allow to pause without PAUSIBLE_ROLE', async() => {
        const seedToken = await EESeedToken.deployed()
        await truffleAssert.fails(
            seedToken.pause.sendTransaction({ from: CLIENT }),
            null,
            "ERC20PresetMinterPauser: must have pauser role to pause"
        );
    })

    it('should allow tranfers to whitelisted wallets', async() => {
        const seedToken = await EESeedToken.deployed()
        await seedToken.mint(CLIENT, ether('2'), { from: ADMIN })
        await seedToken.transfer(BENEFICIARY, ether('1'), { from: CLIENT })
        assert.equal(await seedToken.balanceOf.call(BENEFICIARY).then(b => b.toString()), ether('1'), "invalid transfer")
        await seedToken.approve.sendTransaction(RANDOM, ether('1'), { from: CLIENT })
        await seedToken.transferFrom(CLIENT, BENEFICIARY, ether('1'), { from: RANDOM })
        assert.equal(await seedToken.balanceOf.call(BENEFICIARY).then(b => b.toString()), ether('2'), "invalid transfer")
    })

    it('should not allow tranfers to other wallets', async() => {
        const seedToken = await EESeedToken.deployed()
        await seedToken.mint(CLIENT, ether('1'), { from: ADMIN })

        await truffleAssert.fails(
            seedToken.transfer(ADMIN, ether('1'), { from: CLIENT }),
            null,
            "Recipient is not allowed to recieve"
        );

        await seedToken.approve.sendTransaction(RANDOM, ether('1'), { from: CLIENT })
        await truffleAssert.fails(
            seedToken.transferFrom(CLIENT, ADMIN, ether('1'), { from: RANDOM }),
            null,
            "Recipient is not allowed to recieve"
        );
    })
})