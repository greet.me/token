const fs = require("fs")
const path = require("path")
const HDWalletProvider = require("@truffle/hdwallet-provider")
const ethers = require("ethers")

const MNEMONIC = 'nerve panda scare join quantum reform ski umbrella clap worth barely ask' // Testable mnemonic
const provider = new HDWalletProvider({
    mnemonic: {
        phrase: MNEMONIC
    },
    //addressIndex: 4,
    numberOfAddresses: 6,
    providerOrUrl: "http://localhost:8545"
});
web3.setProvider(provider)

function parseSimpleConfig(config, defaults = {}) {
    return config.reduce((acc, arg) => {
        [key, value] = arg.split('=')
        acc[key.slice(2)] = value.trim()
        return acc
    }, defaults)
}

function domain(chainId, verifyingContract) {
    console.log(verifyingContract)
    return {
        name: "AccessManager",
        version: "v1",
        chainId,
        verifyingContract
    }
}

module.exports = async function(callback) {
    try {
        const Swap = artifacts.require("Swap")
        const config = parseSimpleConfig(process.argv.slice(4))
        const chainId = 1 //await web3.eth.net.getId() // Yes, it's not the same
        const network = await web3.eth.net.getId()

        const file = fs.readFileSync(path.resolve(__dirname, config.file_with_address), "utf8")
        const addresses = file.split(",").map((address) => address.trim())
        const contractDomain = domain(chainId, network == 5777 ? provider.getAddress(5) : Swap.address)

        const signaturePackageSize = 10;
        for (let i = 0; i <= addresses.length / signaturePackageSize; i++) {
            const signatures = [];
            console.log("Creating signatures for the ", i, "-th package");
            for (const address of addresses.slice(
                    i * signaturePackageSize,
                    (i + 1) * signaturePackageSize,
                )) {
                const auctioneerMessage = ethers.utils.keccak256(
                    ethers.utils.defaultAbiCoder.encode(
                        ["bytes32", "address"], [
                            ethers.utils._TypedDataEncoder.hashDomain(
                                contractDomain,
                            ),
                            address
                        ],
                    ),
                );

                const auctioneerSignature = await web3.eth.sign(
                    auctioneerMessage.toString(),
                    provider.getAddress(0)
                );
                const sig = ethers.utils.splitSignature(
                    auctioneerSignature,
                );
                const auctioneerSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
                    ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
                );
                signatures.push({
                    user: address,
                    signature: auctioneerSignatureEncoded,
                });
            }
            const json = JSON.stringify({
                chainId: chainId,
                allowListContract: Swap.address,
                signatures: signatures,
            });
            console.log(json)
        }

        provider.engine.stop();

        callback(signatures)
    } catch (e) {
        console.error(e)
    }
}