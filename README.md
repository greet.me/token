# EE token smart contracts

## token/EESeedToken.sol
ERC-20 token with custom ERC-1404 implementation to limit transfers to whitelisted contract address, which will be used for the future conversion to Greet ecosystem token (EE).

## Swap.sol
Mints ERC-20 seed tokens for payment token contribution (USDC) by a fixed exchange rate. All contributed tokens go straight to [Greet DAO multisig](https://gnosis-safe.io/app/#/safes/0x96Cfb7a2B265575EAf0aa1949263AeD63a4D31c0/balances).

Uses whitelisting businesses logic for participation, inspired by [Gnosis Auction code](https://github.com/gnosis/ido-contracts/blob/main/contracts/allowListExamples/AllowListOffChainManaged.sol) and the [following article](https://medium.com/@PhABC/off-chain-whitelist-with-on-chain-verification-for-ethereum-smart-contracts-1563ca4b8f11).

# Local installation
Expects Ganache or similar Ethereum client on `127.0.0.1:8545`, default `network_id` is 5777.

    npm install
    npm run migrate:dev

# Testing
Signatures for whitelisting can be generated with the following cli command: `truffle exec tasks/generate_signatures.js --network=develop --file_with_address="../your_address_inputs.txt"`

`your_address_inputs.txt` contains coma separated whitelisted wallet. Can be generated with [Greet P2P KYC tool](https://kyc.greet.me).

Default KYC signer wallet used for tests is `0x4253e41227F7af597176963A4F1e8399539e60D5`, it's derived from `nerve panda scare join quantum reform ski umbrella clap worth barely ask` test mnemonic. _Don't use it for anything other than local tests, it's insecure._

`tasks/generate_signatures.js` you can replace test mnemonic with your local Ganache mnemonic, in such case first wallet `provider.getAddress(0)` will be a whitelisting KYC signer.

You may need to update `CLIENT_WHITELIST_SIGNATURE` and `CLIENT_NO_MONEY_WHITELIST_SIGNATURE` in `test/seed_token.js` and `KYC_SIGNER` in `migrations/2_deploy_seed_token.js` for a local signer wallet. Alternatively `KYC_SIGNER` can be updated on a smart contract level, like: `swap.updateContractData.sendTransaction("YOUR_ABI_ENCODED_KYC_SIGNER_WALLET", ether("50000"), ether("150000"), { from: ADMIN })`.

To run automatic tests: 

    npm run test

# Deployments

## Mainnet

* EESeedToken: 0x77c2F5cB9f191af74FeB1669A8cC4774c654C68E
* Swap: 0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD

## Rinkeby

* EESeedToken: 0xBbbe3249AADbafC155Ada7D82731346e43531D30
* PaymentTokenMock: 0x08E89A5f292398cc1581655592E2E6C9c11Ab272
* DaiTokenMock: 0x259137a40745D35a0fbe6aD8c89eA1e611c303F4
* Swap: 0x3909bD2ac33bBd341f6eED7f272785B6B88319Fc

# Direct interactions with smart contract by DAO

1) Provide address which will call smart contract to Greet team and get confirmation that it's whitelisted and `SIGNATURE` value;
2) Send `approve(0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD, 250000000000)` transaction to `0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48`;
3) Send `contribute(250000000000, 0xd183d7aadc13ca77a0189b5355e86039be51773704ce672f08afbfb1527400e9, SIGNATURE)` transaction to `0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD`;
4) Check ERC20 balance of seedEE for `0x77c2F5cB9f191af74FeB1669A8cC4774c654C68E` token address.